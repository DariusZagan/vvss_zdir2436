package biblioteca;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;

import java.util.ArrayList;
import java.util.List;

public class Service {

    private CartiRepoInterface repo;

    public Service(CartiRepoInterface cr) {
        this.repo = cr;
    }

    public void adaugaCarte(Carte c) {
        repo.adaugaCarte(c);
    }

    public List<Carte> getCarti() {
        return repo.getCarti();
    }

    public List<Carte> cautaCarte(String ref) {
        List<Carte> lc = new ArrayList<>();

        for (Carte c : repo.getCarti()) {
            for(String r : c.getReferenti() ){
                if (r.equals(ref) || r.contains(ref))
                    lc.add(c);
            }
        }
        return lc;
    }

    public List<Carte> getCartiOrdonateDinAnul(String an) {

        return repo.getCartiOrdonateDinAnul(an);
    }
}
