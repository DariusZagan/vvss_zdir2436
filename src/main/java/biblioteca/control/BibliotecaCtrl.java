package biblioteca.control;

import biblioteca.Service;
import biblioteca.model.Carte;
import biblioteca.util.Validator;


import java.util.List;

public class BibliotecaCtrl {

    private Service service;

    public BibliotecaCtrl(Service service) {
        this.service = service;
    }

    public void adaugaCarte(String titlu, Integer anAparitie, List<String> referenti, List<String> cuvinteCheie) throws Exception {
        Carte c = new Carte();
        c.setAnAparitie(anAparitie);
        c.setTitlu(titlu);
        c.setReferenti(referenti);
        c.setCuvinteCheie(cuvinteCheie);

        Validator.validateCarte(c);
        service.adaugaCarte(c);
    }

//    public void adaugaCarte(Carte c) throws Exception {
//        Validator.validateCarte(c);
//        service.adaugaCarte(c);
//    }

    public List<Carte> cautaCarte(String autor) throws Exception {
        Validator.isStringOK(autor);
        return service.cautaCarte(autor);
    }

    public List<Carte> getCarti() {
        return service.getCarti();
    }

    public List<Carte> getCartiOrdonateDinAnul(String an) throws Exception {
        if (!Validator.isNumber(an))
            throw new Exception("Nu e numar!");
        return service.getCartiOrdonateDinAnul(an);
    }


}
