package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Test {

    private BibliotecaCtrl bibliotecaCtrl;
    private Service service;
    private CartiRepoMock cartiRepoMock;

    private List<String> list = new ArrayList<>();


    @Before
    public void setUp(){
        cartiRepoMock = new CartiRepoMock();
        service = new Service(cartiRepoMock );
        bibliotecaCtrl = new BibliotecaCtrl(service);
    }


    @org.junit.Test
    public void cautaCarte() throws Exception {
        List<Carte> carti = bibliotecaCtrl.cautaCarte("Caragiale");
        assertEquals(carti.size(), 3);
    }


    @org.junit.Test
    public void cautaCarteNonvalid2() throws Exception {
        cartiRepoMock.getCarti().clear();
        List<Carte> carti = bibliotecaCtrl.cautaCarte("Caragiale");
        assertEquals(carti.size(), 0);
    }


    @org.junit.Test(expected = Exception.class)
    public void cautaCarteNonvalid() throws Exception {

        List<Carte> carti = bibliotecaCtrl.cautaCarte("");
    }


}

