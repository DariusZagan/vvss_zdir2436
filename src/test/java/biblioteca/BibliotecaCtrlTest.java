package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class BibliotecaCtrlTest {

    private BibliotecaCtrl bibliotecaCtrl;
    private Service service;
    private CartiRepoMock cartiRepoMock;

    private List<String> list = new ArrayList<>();


    @Before
    public void setUp() {
        cartiRepoMock = new CartiRepoMock();
        service = new Service(cartiRepoMock);
        bibliotecaCtrl = new BibliotecaCtrl(service);
        cartiRepoMock.getCarti().clear();
    }

    @Test
    public void adaugaCarte() throws Exception {

        bibliotecaCtrl.adaugaCarte("Salut", 1956, list, list);
        assertEquals(cartiRepoMock.getCarti().size(), 1);
    }


    @Test(expected = Exception.class)
    public void adaugaCarte2() throws Exception {

        bibliotecaCtrl.adaugaCarte("Salut!", 1956, list, list);
        assertEquals(cartiRepoMock.getCarti().size(), 0);
    }


    @Test(expected = Exception.class)
    public void adaugaCarte3() throws Exception {

        bibliotecaCtrl.adaugaCarte("Salut", 1949, list, list);
        assertEquals(cartiRepoMock.getCarti().size(), 0);
    }

    @Test
    public void adaugaCarte4() throws Exception {

        bibliotecaCtrl.adaugaCarte("Salut", 1951, list, list);
        assertEquals(cartiRepoMock.getCarti().size(), 1);
    }

}
