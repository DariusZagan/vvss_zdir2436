package biblioteca;


import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BigBang {
    private static CartiRepoInterface cartiRepo;
    private static Service service;
    private static BibliotecaCtrl controller;
    private static int size;

    @BeforeClass
    public static void init() {
        cartiRepo = new CartiRepoMock();
        service = new Service(cartiRepo);
        controller = new BibliotecaCtrl(service);
        size = controller.getCarti().size();
    }


    @Before
    public void before() {
        cartiRepo.getCarti().clear();
    }

    @Test
    public void A_Test() {
        List<String> referenti = new ArrayList<String>();
        referenti.add("Darius");
        String titlu = "";
        int anAparitie = 2016;

        try {
            controller.adaugaCarte(titlu, anAparitie, referenti,  new ArrayList<>(Arrays.asList("Rapid", "Giulesti")));
            assert false;
        } catch (Exception ignored) {
            assert true;
        }
    }


    @Test
    public void B_Test() throws Exception {
        List<String> referenti = new ArrayList<>();
        referenti.add("Darius");
        String titlu = "Amintiri";
        int anAparitie = 2016;

        controller.adaugaCarte(titlu, anAparitie, referenti,  new ArrayList<>(Arrays.asList("Rapid", "Giulesti")));
        List<Carte> carti = controller.cautaCarte("Darius");
        assert carti.size() == 1;
        assert carti.get(0).getReferenti().get(0).equals("Darius");
    }

    @Test
    public void C_Test() throws Exception {
        List<String> referenti = new ArrayList<>();
        referenti.add("Darius");
        String titlu = "Amintiri";
        int anAparitie = 2016;

        String titlu1 = "Copilarie";
        List<String> referenti1 = new ArrayList<>();
        referenti1.add("Zagan");

        try {
            controller.adaugaCarte(titlu, anAparitie, referenti,  new ArrayList<>(Arrays.asList("Rapid", "Giulesti")));
            controller.adaugaCarte(titlu1, anAparitie, referenti1,  new ArrayList<>(Arrays.asList("Dinamo", "Bucuresti")));

            assert true;
        } catch (Exception ignored) {
            assert false;
        }

        List<Carte> carti = null;

        try {
            carti = controller.getCartiOrdonateDinAnul(String.valueOf(anAparitie));
            assert true;
        } catch (Exception e) {
            e.printStackTrace();
            assert false;
        }

        assert carti.size() == 2;
        assert carti.get(0).getTitlu().equals("Amintiri");
        assert carti.get(1).getTitlu().equals("Copilarie");
    }

    @Test
    public void A_B_C_Test() {
        List<String> referenti = new ArrayList<>();
        referenti.add("Darius");
        String titlu = "Amintiri";
        int anAparitie = 2016;

        String titlu1 = "Copilarie";
        List<String> referenti1 = new ArrayList<>();
        referenti1.add("Zagan");

        //F01 adauga
        try {
            controller.adaugaCarte(titlu, anAparitie, referenti,  new ArrayList<>(Arrays.asList("Rapid", "Giulesti")));
            controller.adaugaCarte(titlu1, anAparitie, referenti1,  new ArrayList<>(Arrays.asList("Dinamo", "Bucuresti")));

            assert true;
        } catch (Exception ignored) {
            assert false;
        }

        //F02 cautare dupa autor
        List<Carte> carti = null;
        try {
            carti = controller.cautaCarte("Zagan");
            assert true;
        } catch (Exception ignored) {
            assert false;
        }
        assert carti.size() == 1;
        assert carti.get(0).getReferenti().get(0).equals("Zagan");

        //F03 cautare dupa an aparitie si sortare dupa autori
        carti.clear();

        try {
            carti = controller.getCartiOrdonateDinAnul(String.valueOf(anAparitie));
            assert true;
        } catch (Exception e) {
            e.printStackTrace();
            assert false;
        }

        assert carti.size() == 2;
        assert carti.get(0).getTitlu().equals("Amintiri");
        assert carti.get(1).getTitlu().equals("Copilarie");
    }
}