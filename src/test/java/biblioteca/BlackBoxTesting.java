package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BlackBoxTesting {

    private static CartiRepoInterface cartiRepo;
    private static Service service;
    private static BibliotecaCtrl controller;
    private static int size;

    @BeforeClass
    public static void init() {
        cartiRepo = new CartiRepoMock();
        service = new Service(cartiRepo);
        controller = new BibliotecaCtrl(service);
        size = controller.getCarti().size();
    }

    @Test
    public void Valid() {
        List<String> referenti = new ArrayList<>();
        referenti.add("ZaganDarius");
        String titlu1 = "Mybook";
        int anAparitie = 2000;

        String titlu2 = "MyBook";
        List<String> referenti2 = new ArrayList<>();
        referenti2.add("Zagan");

        try {
            controller.adaugaCarte(titlu1, anAparitie, referenti, new ArrayList<>(Arrays.asList("Rapid", "Giulesti")));
            controller.adaugaCarte(titlu2, anAparitie, referenti2, new ArrayList<>(Arrays.asList("Dinamo", "Bucuresti")));
            assert true;
        } catch (Exception ignored) {
            System.out.println(ignored);
            assert false;
        }

        List<Carte> carti = null;

        try {
            carti = controller.getCartiOrdonateDinAnul(String.valueOf(anAparitie));
            assert true;
        } catch (Exception e) {
            e.printStackTrace();
            assert false;
        }

        assert carti.size() == 2;
        assert carti.get(0).getTitlu().equals("MyBook");
        assert carti.get(1).getTitlu().equals("Mybook");
    }


    @Test
    public void NonValid() {
        List<String> referenti = new ArrayList<>();
        referenti.add("ZaganDarius");
        String titlu1 = "Mybook";
        int anAparitie = 2000;

        String titlu2 = "MyBook";
        List<String> referenti2 = new ArrayList<>();
        referenti2.add("Zagan");

        try {
            controller.adaugaCarte(titlu1, anAparitie, referenti, new ArrayList<>(Arrays.asList("Rapid", "Giulesti")));
            controller.adaugaCarte(titlu2, anAparitie, referenti2, new ArrayList<>(Arrays.asList("Dinamo", "Bucuresti")));
            assert true;
        } catch (Exception ignored) {
            System.out.println(ignored);
            assert false;
        }

        List<Carte> carti = null;


        try {
            carti = controller.getCartiOrdonateDinAnul("anAparitie");
            assert false;
        } catch (
                Exception e) {
            assert true;
        }
    }
}